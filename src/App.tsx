import React from 'react';
import TodoList from './store/components/TodoList';
import UserList from './store/components/UserList';

const App = () => {
  return (
    <div>
      <UserList />
      <hr />
      <TodoList />
    </div>
  );
};

export default App;
